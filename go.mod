module golang-uts-pti-server-1stbatch

go 1.13

require (
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.6.2
)
