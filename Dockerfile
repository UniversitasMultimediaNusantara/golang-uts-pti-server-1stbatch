ARG ALPINE_VERSION=3.11.2
ARG BUILDER_VERSION=1.13.5-alpine3.11

#BUILDER
FROM golang:${BUILDER_VERSION} as builder

RUN apk update && apk upgrade && \
    apk --no-cache --update add git make && \
    go get -u github.com/golang/dep/cmd/dep

WORKDIR /go/src/withered-flowers/PTIServer1stBatch

COPY . .

RUN dep ensure -v && go build -o PTI1stBatch main.go

#RELEASE
FROM alpine:${ALPINE_VERSION}

RUN apk update && apk upgrade && \
    apk --no-cache --update add ca-certificates tzdata && \
    mkdir /app

WORKDIR /app

EXPOSE 15000

COPY assets/ assets/
COPY web/ web/
#COPY hiddenassets/ hiddenassets/
#COPY templates/ templates/
COPY --from=builder /go/src/withered-flowers/PTIServer1stBatch/PTI1stBatch /app

CMD /app/PTI1stBatch

